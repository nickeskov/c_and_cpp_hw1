#ifndef HW1_H
# define HW1_H

# include <string.h>

int    ***split_by_n(const int **matr, size_t rows, size_t cols, size_t n);

#endif
