#!/bin/sh
# execute after run_tests.sh
cp build/CMakeFiles/mytests.dir/test/test.c.gcda ./test.gcda
cp build/CMakeFiles/mytests.dir/test/test.c.gcno ./test.gcno
cp build/CMakeFiles/mytests.dir/src/hw1.c.gcda ./hw1.gcda
cp build/CMakeFiles/mytests.dir/src/hw1.c.gcno ./hw1.gcno

gcovr -r . --html --html-details -o code_coverage_report.html
rm -f *.gcda *.gcno
