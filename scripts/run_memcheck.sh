#!/bin/sh
mkdir -p build && cd build
cmake ..
make
valgrind --tool=memcheck --leak-check=full --log-file=../memcheck.log \
    ./mytests < ../test.txt
