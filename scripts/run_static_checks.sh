#!/bin/sh
mkdir -p build && cd build
cmake .. -DENABLE_CPPCHECK=ON
make check
