#include <stdlib.h>
#include <stdbool.h>

#include "hw1.h"

static int  **init_matrix(size_t rows, size_t cols)
{
    int **matr = NULL;

    if (rows != 0 && cols != 0)
    {
        matr = (int **) malloc(rows * sizeof(*matr));
        if (matr != NULL)
        {
            int *matr_elems = (int *) malloc(rows * cols * sizeof(**matr));
            if (matr_elems != NULL)
                for (size_t i = 0; i < rows; ++i)
                   matr[i] = matr_elems + cols * i;
            else
            {
                free(matr);
                matr = NULL;
            }
        }
    }
    return (matr);
}

static int  ***init_matrix_arr(size_t rows, size_t cols, size_t n)
{
    int ***res = NULL;

    res = (int ***) malloc(n * sizeof(*res));
    if (res != NULL)
        for (size_t i = 0; i < n; ++i)
        {
            int extra_col = 0;
            if (cols % n != 0)
            {
                extra_col = 1;
                --cols;
            }
            res[i] = init_matrix(rows, cols / n + extra_col);
            if (i < cols && res[i] == NULL)
            {
                for (size_t j = 0; j < i; ++j)
                    free(res[j]);
                free(res);
                return (NULL);
            }
        }
    return (res);
}

static bool check_params(const int **matr, const size_t rows,
                         const size_t cols, const size_t n)
{
    if (matr != NULL && rows != 0 && cols != 0 && n != 0)
    {
        for (size_t i = 0; i < rows; ++i)
            if (matr[i] == NULL)
                return (false);
        return (true);
    }
    return (false);
}

int          ***split_by_n(const int **matr, size_t rows, size_t cols,
                           size_t n)
{
    int ***res = NULL;

    if (check_params(matr, rows, cols, n))
    {
        res = init_matrix_arr(rows, cols, n);
        for (size_t i = 0; i < rows; ++i)
            for (size_t j = 0; j < cols; ++j)
                res[j % n][i][j / n] = matr[i][j];
    }
    return (res);
}
