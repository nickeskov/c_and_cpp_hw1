#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "hw1.h"

static int  **init_matr(size_t rows, size_t cols)
{
    int **matr = NULL;

    if (rows != 0 && cols != 0)
    {
        matr = (int **) malloc(rows * sizeof(*matr));
        if (matr != NULL)
            for (size_t i = 0; i < rows; ++i)
            {
                matr[i] = (int *) malloc(cols * sizeof(**matr));
                if (matr[i] == NULL)
                {
                    for (size_t j = 0; j < i; ++j)
                        free(matr[j]);
                    free(matr);
                    return (NULL);
                }
            }
    }
    return (matr);
}

static void free_matr(int **matr, size_t rows)
{
    if (matr != NULL)
    {
        for (size_t i = 0; i < rows; ++i)
            free(matr[i]);
        free(matr);
    }
}

static void cleanup(int ***marr, size_t n)
{
    if (marr)
        for (size_t i = 0; i < n; ++i)
        {
            if (marr[i])
                free(marr[i][0]);
            free(marr[i]);
        }
    free(marr);
}

static bool fill_matr(int **matr, size_t rows, size_t cols)
{
    if (matr != NULL)
        for (size_t i = 0; i < rows; ++i)
            for (size_t j = 0; j < cols; ++j)
            {
                if (scanf("%d\n", matr[i] + j) != 1)
                    return (false);
            }
    return (true);
}

typedef struct test_data {
    size_t  rows;
    size_t  cols;
    int     **matr;
    int     is_valid;
} test_data_t;

test_data_t read_data(void)
{
    test_data_t tdata;

    tdata.rows = 0;
    tdata.cols = 0;
    tdata.matr = NULL;
    tdata.is_valid = 0;

    if (scanf("%d %zu %zu\n", &(tdata.is_valid),
            &(tdata.rows), &(tdata.cols)) != 3)
        return (tdata);
    if ((tdata.matr = init_matr(tdata.rows, tdata.cols)) == NULL ||
        !fill_matr(tdata.matr, tdata.rows, tdata.cols))
        return (tdata);
    return (tdata);
}

int         test(test_data_t tdata)
{
    if (tdata.is_valid)
    {
        int     ***marr = split_by_n((const int **)tdata.matr, tdata.rows, tdata.cols, 3);

        for (size_t i = 0; i < tdata.rows; ++i)
        {
            for (size_t j = 0; j < tdata.cols; ++j)
            {
                if (marr[j % 3] != NULL && marr[j % 3][i][j / 3] != tdata.matr[i][j])
                {
                    cleanup(marr, 3);
                    return (0);
                }
            }
        }
        cleanup(marr, 3);
    }
    else
    {
        if (split_by_n((const int **) tdata.matr, tdata.rows, tdata.cols, 3) != NULL)
            return (0);
    }
    return (1);
}

int         main(void)
{
    size_t  tests_count = 0;

    if (scanf("%zu", &tests_count) != 1)
        return (1);

    for (size_t i = 0; i < tests_count; ++i)
    {
        test_data_t tdata = read_data();
        if (test(tdata) != 1)
        {
            free_matr(tdata.matr, tdata.rows);
            return (1);
        }
        free_matr(tdata.matr, tdata.rows);
    }
    return (0);
}
