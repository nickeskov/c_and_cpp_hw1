#include <gtest/gtest.h>

extern "C" {
# include "hw1.h"
}

/*
#include <stdlib.h>
static void cleanup(int ***marr, size_t n)
{
    if (marr)
        for (size_t i = 0; i < n; ++i)
        {
            if (marr[i])
                free(marr[i][0]);
            free(marr[i]);
        }
    free(marr);
}
*/
TEST(SplitByN, InvalidData)
{
   int matr_invalid[][3] = {
       {0, 1, 2},
       {0, 1, 2},
       {0, 1, 2}
   };
   int matr_valid[][4] = {
       {0, 1, 2, 3},
       {0, 1, 2, 3},
       {0, 1, 2, 3},
       {0, 1, 2, 3}
   };

   const int **matr = (const int **) matr_valid;
   ASSERT_EQ(NULL, split_by_n(NULL, 3, 3, 3));
   ASSERT_EQ(NULL, split_by_n((const int **)matr_invalid, 3, 3, 3));
   ASSERT_EQ(NULL, split_by_n(matr, 0, 3, 3));
   ASSERT_EQ(NULL, split_by_n(matr, 3, 0, 3));
   ASSERT_EQ(NULL, split_by_n(matr, 3, 3, 0));
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
